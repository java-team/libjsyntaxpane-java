#!/bin/sh

set -ex

BRANCH=r096
UPSTREAM_VERSION=0.9.6

TEMP_DIR=`mktemp -d /tmp/libjsyntaxpane-java-tgz.XXXXXXXXX`
WHERE=`pwd`
trap "cd $WHERE; rm -rf $TEMP_DIR" INT EXIT

cd $TEMP_DIR
svn checkout http://jsyntaxpane.googlecode.com/svn/branches/$BRANCH/ libjsyntaxpane-java-branch$BRANCH
cd libjsyntaxpane-java-branch$BRANCH
REVISION=`LC_ALL=C svn info |grep Revision:|sed 's/Revision: //'`
mv jsyntaxpane libjsyntaxpane-java-$UPSTREAM_VERSION~r$REVISION
tar zvcf $WHERE/libjsyntaxpane-java_$UPSTREAM_VERSION~r$REVISION.orig.tar.gz  --exclude-vcs libjsyntaxpane-java-$UPSTREAM_VERSION~r$REVISION
rm -rf src

return 0
